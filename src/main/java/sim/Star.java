package sim;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Sphere;

public class Star extends NewtonianObject {
	private static final Logger logger = LogManager.getLogger();
	// private Node node;
	private Geometry geom;
	private static Random rgen = new Random();

	public Star(AssetManager assetManager, Vector3f position, Vector3f velocity, float mass, ColorRGBA color) {
		super(position, velocity, mass, color);
		// node = new Node();

		// Disk disk = new Disk(8, Physics.starRadius);
		float rad = Physics.starRadius * (rgen.nextFloat() + 0.5f) * 0.35f;
		Sphere sphere = new Sphere(19, 17, rad);
		geom = new Geometry("Star", sphere);

		Material sphereMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		color.a = 0.5f;
		sphereMat.setColor("Color", color);
		sphereMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);

		// sphereMat.setBoolean("UseMaterialColors", true);

		geom.setMaterial(sphereMat);

		geom.setQueueBucket(Bucket.Transparent);
	}

	public void dump() {
		logger.info(getPositionVelocityString());
	}

	// public Node getNode() {
	// return node;
	// }

	public Geometry getGeom() {
		return geom;
	}

}
