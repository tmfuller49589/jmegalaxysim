package sim;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;

public class NewtonianObject {
	private static final Logger logger = LogManager.getLogger();

	protected Vector3f velocity;
	protected Vector3f position;
	protected Vector3f acceleration = new Vector3f(0f, 0f, 0f);

	protected float mass;
	protected ColorRGBA color = ColorRGBA.White;

	public NewtonianObject(Vector3f position, Vector3f velocity, float mass, ColorRGBA color) {
		this.velocity = velocity;
		this.position = position;
		this.mass = mass;
		this.color = color;
	}

	public NewtonianObject(float mass, ColorRGBA color) {
		this.velocity = new Vector3f(0f, 0f, 0f);
		this.position = new Vector3f(0f, 0f, 0f);
		this.mass = mass;
		this.color = color;
	}

	public NewtonianObject() {
		this.velocity = new Vector3f(0f, 0f, 0f);
		this.position = new Vector3f(0f, 0f, 0f);
	}

	public String getPositionVelocityString() {
		String s = position.getX() + ", ";
		s += position.getY() + ", ";
		s += position.getZ() + ", ";
		s += velocity.getX() + ", ";
		s += velocity.getY() + ", ";
		s += velocity.getZ() + ", ";
		s += acceleration.getX() + ", ";
		s += acceleration.getY() + ", ";
		s += acceleration.getZ() + ", ";
		s += mass;
		return s;
	}

	public Vector3f getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector3f velocity) {
		this.velocity = velocity;
	}

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public float getMass() {
		return mass;
	}

	public void setMass(float mass) {
		this.mass = mass;
	}

	public ColorRGBA getColor() {
		return color;
	}

	public void setColor(ColorRGBA color) {
		this.color = color;
	}

	public Vector3f getAcceleration() {
		return acceleration;
	}

	public void setAcceleration(Vector3f acceleration) {
		this.acceleration = acceleration;
	}
}
