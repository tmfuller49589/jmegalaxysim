package sim;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jme3.math.Vector3f;

import jme3.JGalaxy;

public class Physics {

	public enum IntegrationMethodEnum {
		euler, leapfrog, yoshida;
	}

	int maxThreads = 16;
	Thread[] threads = new Thread[maxThreads];
	// MandelbrotThread[] mandelbrotThreads = new MandelbrotThread[maxThreads];

	private static IntegrationMethodEnum integrationMethod = IntegrationMethodEnum.leapfrog;
	private static boolean firstMove = true;

	public static Float rSoft = 0.05f;
	public static final float G = 1.0f;
	private static final Logger logger = LogManager.getLogger();
	public static final float limit = 2f;
	public static final float xmin = -limit;
	public static final float xmax = limit;
	public static final float ymin = -limit;
	public static final float ymax = limit;
	public static final float zmin = -limit;
	public static final float zmax = limit;
	public static final float vscale = .5f;
	public static final float minGalaxyMass = 1e5f;
	public static final float maxGalaxyMass = 2e5f;
	// public static final int minStars = 050000;
	// public static final int maxStars = 200001;
	public static final int minStars = 07000;
	public static final int maxStars = 10000;
	public static final float starRadius = 0.05f;

	public static final float maxGalaxyRadius = 1f;
	public static final float minGalaxyRadius = rSoft * 1.5f;
	public static final float minStarMass = .1f;
	public static final float maxStarMass = 10f;
	// this dt corresponds to 20 steps per orbit of particle at r=rSoft
	public static float dt = (float) (2.0 * Math.PI * rSoft / 20d * Math.sqrt(rSoft / G / maxGalaxyMass));
	public static float time = 0;

	public static final Random randGen = new Random();
	private static BufferedWriter particleDatawriter = null;
	private static BufferedWriter initialStarDatawriter = null;
	private static final Boolean writeParticleData = false;
	private static String particleDataFileName = null;
	private static final int reportingInterval = 0;
	private static int reportingIdx = 0;

	public static float randomVelocityFactor = .01f;

	public IntegrationMethodEnum getIntegrationMethod() {
		return integrationMethod;
	}

	public static void setIntegrationMethod(IntegrationMethodEnum integrationMethod) {
		Physics.integrationMethod = integrationMethod;
		particleDataFileName = integrationMethod.name() + ".csv";
		if (writeParticleData) {
			openParticleDataFile();
		}
	}

	public static void openParticleDataFile() {
		FileWriter file;
		try {
			file = new FileWriter(particleDataFileName);
			particleDatawriter = new BufferedWriter(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void closeParticleDataFile() {
		try {
			if (particleDatawriter != null) {
				logger.info("closing particle data file");
				particleDatawriter.close();
			}
			if (initialStarDatawriter != null) {
				logger.info("closing initial star data file");
				initialStarDatawriter.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void writeParticleData(JGalaxy[] galaxies) {
		if (!writeParticleData) {
			return;
		}
		++reportingIdx;
		if (reportingIdx < reportingInterval) {
			return;
		}
		reportingIdx = 0;
		try {
			ArrayList<ParticleData> particleDataList = Physics.computeEnergy(galaxies);

			for (ParticleData pd : particleDataList) {
				particleDatawriter
						.write(pd.getNewtonianObject().getPositionVelocityString() + ", " + pd.toCsv() + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static ParticleData computeEnergy(NewtonianObject o1, NewtonianObject o2) {
		float dist = o1.getPosition().distance(o2.getPosition()) + rSoft;
		float v = o2.getVelocity().length();
		float Eg = -G * o1.getMass() * o2.getMass() / dist;
		float Ek = 0.5f * o2.getMass() * v * v;
		float Et = Eg + Ek;

		ParticleData pd = new ParticleData(o2, time, Et, Eg, Ek, dist, v);

		return pd;

	}

	public static ArrayList<ParticleData> computeEnergy(JGalaxy[] galaxies) {
		ArrayList<ParticleData> particleDataList = new ArrayList<ParticleData>();
		for (JGalaxy g : galaxies) {
			for (Star s : g.getStars()) {
				ParticleData pd = computeEnergy(g, s);
				particleDataList.add(pd);
			}
		}
		return particleDataList;
	}

	public static Vector3f computeForce(NewtonianObject o1, NewtonianObject o2) {

		float dist = o1.getPosition().distance(o2.getPosition()) + rSoft;
		Vector3f r = o2.getPosition().subtract(o1.getPosition());
		Vector3f force = r.mult((float) (-G * o1.getMass() * o2.getMass() / Math.pow(dist, 3)));

		// logger.info(" o1 pos = " + o1.getPosition() + " o2 pos = " + o2.getPosition()
		// + " force = " + force.toString()
		// + " m1 = " + o1.getMass() + " m2 = " + o2.getMass() + " dist = " + dist + " r
		// = " + r.toString());
		return force;
	}

	public static Vector3f accel(NewtonianObject o1, NewtonianObject o2) {
		Vector3f force = computeForce(o1, o2);
		Vector3f accel = force.mult(1f / o2.getMass());
		return accel;
	}

	private static void dumpVec(String name, float[] vec) {
		logger.info(name + " = " + vec[0] + " " + vec[1] + " " + vec[2]);
	}

	public static void move(JGalaxy[] galaxies) {
		switch (integrationMethod) {
		case euler:
			moveEulerIntegration(galaxies);
			time += dt;
			break;
		case leapfrog:
			if (firstMove) {
				moveLeapfrogFirstStep(galaxies);
				firstMove = false;
			} else {
				moveLeapfrog(galaxies);
				time += dt;
			}
			break;
		case yoshida:
			moveLeapfrogYoshida(galaxies);
			time += dt;
			break;
		}

		// logger.info("time = " + time);
	}

	public static float getTime() {
		return time;
	}

	public static void updateAccels(JGalaxy[] galaxies) {
		// calculate force between galaxies
		for (JGalaxy g1 : galaxies) {
			Vector3f force = new Vector3f(0f, 0f, 0f);
			for (JGalaxy g2 : galaxies) {
				if (g1 != g2) {
					force = force.add(computeForce(g2, g1));
				}
			}
			g1.setForce(force);
		}

		// calculate force on stars from galaxy black holes
		for (Galaxy galaxy : galaxies) {
			for (Star star : galaxy.stars) {
				Vector3f force = new Vector3f(0f, 0f, 0f);
				for (Galaxy galGrav : galaxies) {
					force = force.add(computeForce(galGrav, star));
				}

				galaxy.setForce(galaxy.getForce().add(force.mult(-1f)));
				Vector3f accel = force.mult(1f / star.getMass());
				star.setAcceleration(accel);
			}
			Vector3f accel = galaxy.getForce().mult((float) (1. / galaxy.getMass()));
			galaxy.setAcceleration(accel);
		}
	}

	public static void moveLeapfrogFirstStep(JGalaxy[] galaxies) {
		// Calculate the velocity at the first half step
		// v_{i + 1/2} = v_{i - 1/2} + a_i * dt
		// x_{i + 1} = x_i + v_{i +1/2} * dt

		updateAccels(galaxies);

		// update the velocities to the first half step
		for (Galaxy g : galaxies) {

			g.setVelocity(g.getVelocity().add(g.getAcceleration().mult(dt / 2f)));

			for (Star s : g.getStars()) {
				s.setVelocity(s.getVelocity().add(s.getAcceleration().mult(dt / 2f)));
			}
		}
	}

	public static void moveLeapfrogYoshida(JGalaxy[] galaxies) {
		final double c[] = new double[4];
		final double d[] = new double[4];
		final double w0 = (-Math.pow(2d, 1d / 3d) / (2d - Math.pow(2d, 1d / 3d)));
		final double w1 = (1d / (2d - Math.pow(2d, 1d / 3d)));
		c[0] = (w1 / 2d);
		c[1] = ((w0 + w1) / 2d);
		c[2] = c[0];
		c[3] = c[1];
		d[0] = w1;
		d[1] = w0;
		d[2] = w1;
		d[3] = 0d;

		// x1 = x_i + c1 v dt
		// v1 = v_i + d1 a(x1) dt
		// x2 = x1 + c2 v1 dt
		// v2 = v1 + d2 a(x2) dt
		// x3 = x2 + c3 v2 dt
		// v3 = v2 + d3 a(x3) dt

		// x{i+1} = x3 + c4 v3 dt
		// v{i+1} = v3

		for (int i = 0; i < 3; ++i) {
			yoshidaStep(galaxies, c[i], d[i]);
		}
	}

	private static void yoshidaStep(JGalaxy[] galaxies, double c, double d) {
		// update positions
		for (Galaxy g : galaxies) {
			g.setPosition(g.getPosition().add(g.getVelocity().mult((float) (dt * c))));
			for (Star s : g.getStars()) {
				s.setPosition(s.getPosition().add(s.getVelocity().mult((float) (dt * c))));
			}
		}

		// don't update accelerations or velocities if d = 0
		// on last step of Yoshida leapfrog
		if (d != 0) {
			updateAccels(galaxies);

			// update velocities
			for (Galaxy g : galaxies) {
				g.setVelocity(g.getVelocity().add(g.getAcceleration().mult((float) (dt * d))));
				for (Star s : g.getStars()) {
					s.setVelocity(s.getVelocity().add(s.getAcceleration().mult((float) (dt * d))));
				}
			}
		}
	}

	public static void moveLeapfrog(JGalaxy[] galaxies) {
		// v_{i + 1/2} = v_{i - 1/2} + a_i * dt
		// x_{i + 1} = x_i + v_{i +1/2} * dt

		// move the objects
		for (JGalaxy g : galaxies) {
			Vector3f oldPosition = g.getPosition();
			g.setPosition(g.getPosition().add(g.getVelocity().mult(dt)));
			Vector3f positionChange = g.getPosition().subtract(oldPosition);
			g.updateBlackHoleGeometry(positionChange);

			for (Star s : g.getStars()) {
				oldPosition = s.getPosition();
				s.setPosition(s.getPosition().add(s.getVelocity().mult(dt)));
				positionChange = s.getPosition().subtract(oldPosition);
				s.getGeom().setLocalTranslation(s.getPosition());
			}
		}

		updateAccels(galaxies);

		// update velocities
		for (JGalaxy g : galaxies) {

			g.setVelocity(g.getVelocity().add(g.getAcceleration().mult(dt)));

			for (Star s : g.getStars()) {
				s.setVelocity(s.getVelocity().add(s.getAcceleration().mult(dt)));
			}
		}
	}

	public static void moveEulerIntegration(JGalaxy[] galaxies) {

		// calculate force between galaxies
		for (int i = 0; i < galaxies.length; ++i) {
			Vector3f force = new Vector3f(0f, 0f, 0f);
			for (int j = 0; j < galaxies.length; ++j) {
				if (i != j) {
					force = force.add(computeForce(galaxies[j], galaxies[i]));
				}
			}
			Vector3f accel = force.mult(1.f / galaxies[i].getMass());
			galaxies[i].setAcceleration(accel);
		}

		// calculate force on stars from galaxy black holes
		for (JGalaxy galaxy : galaxies) {
			for (Star star : galaxy.getStars()) {
				Vector3f force = new Vector3f(0f, 0f, 0f);
				for (Galaxy galGrav : galaxies) {
					force = force.add(computeForce(galGrav, star));
				}
				Vector3f accel = force.mult(1f / star.getMass());
				star.setAcceleration(accel);
			}
		}

		// move the objects
		for (JGalaxy g : galaxies) {
			Vector3f oldPosition = g.getPosition();
			g.setVelocity(g.getVelocity().add(g.getAcceleration().mult(dt)));
			g.setPosition(g.getPosition().add(g.getVelocity().mult(dt)));
			Vector3f positionChange = g.getPosition().subtract(oldPosition);
			g.updateBlackHoleGeometry(positionChange);

			for (Star s : g.getStars()) {
				s.setVelocity(s.getVelocity().add(s.getAcceleration().mult(dt)));
				s.setPosition(s.getPosition().add(s.getVelocity().mult(dt)));
			}
		}
	}

	public static Vector3f computeCenterOfMass(JGalaxy[] galaxies) {
		Vector3f cm = new Vector3f(0f, 0f, 0f);
		for (JGalaxy g : galaxies) {
			cm = cm.add(g.getPosition().mult(g.getMass()));
		}
		cm = cm.mult(1f / galaxies.length);
		return cm;
	}

	public static void setFirstMove(boolean firstMove) {
		Physics.firstMove = firstMove;
	}

}
