package sim;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;

public class Galaxy extends NewtonianObject {

	protected float radius = 1;
	protected Star[] stars;
	protected float starMass = 1;
	private static final Logger logger = LogManager.getLogger();
	protected ColorRGBA starColor = ColorRGBA.White;
	protected Vector3f force;

	public Galaxy() {
		super();
	}

	public Galaxy(float mass, float radius, float starMass, int numStars, ColorRGBA color, ColorRGBA starColor) {
		super(mass, color);
		this.radius = radius;
		this.stars = new Star[numStars];
		this.starMass = starMass;
		this.starColor = starColor;
	}

	public void dump() {
		logger.info("=============== dumping galaxy ===================");
		for (Star s : stars) {
			s.dump();
		}
		logger.info("==================================================");
	}

	public float getStarMass() {
		return starMass;
	}

	public void setStarMass(float starMass) {
		this.starMass = starMass;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	@Override
	public Vector3f getVelocity() {
		return velocity;
	}

	@Override
	public void setVelocity(Vector3f velocity) {
		this.velocity = velocity;
	}

	@Override
	public Vector3f getPosition() {
		return position;
	}

	@Override
	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public Star[] getStars() {
		return stars;
	}

	public void setStars(Star[] stars) {
		this.stars = stars;
	}

	public ColorRGBA getStarColor() {
		return starColor;
	}

	public void setStarColor(ColorRGBA starColor) {
		this.starColor = starColor;
	}

	public Vector3f getForce() {
		return force;
	}

	public void setForce(Vector3f force) {
		this.force = force;
	}

}
