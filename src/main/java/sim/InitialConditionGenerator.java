package sim;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.math3.geometry.euclidean.threed.SphericalCoordinates;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Transform;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

import jme3.JGalaxy;

public class InitialConditionGenerator {
	private static final Logger logger = LogManager.getLogger();
	private static final String starDataFileName = "starData.txt";
	private JGalaxy[] galaxies;
	private float binarySeparationDistance = 1.0f;

	public enum InitialGalaxyConditionEnum {
		random, binaryOrbit;
	}

	public enum InitialStarConditionEnum {
		randomDisk, linearStarDistribution, oneStar;
	}

	public InitialConditionGenerator(AssetManager assetManager, Node rootNode, int numGalaxies) {
		InitialStarConditionEnum isc = InitialStarConditionEnum.randomDisk;
		int pick = Physics.randGen.nextInt(InitialGalaxyConditionEnum.values().length);
		InitialGalaxyConditionEnum igc = InitialGalaxyConditionEnum.values()[pick];

		if (numGalaxies == 1) {
			igc = InitialGalaxyConditionEnum.random;
		}

		generate(assetManager, igc, isc, numGalaxies);
		for (JGalaxy g : galaxies) {
			g.initStarSpheres(assetManager, rootNode);
		}
	}

	public InitialConditionGenerator(InitialGalaxyConditionEnum initialGalaxyCondition,
			InitialStarConditionEnum initialStarCondition, int numGalaxies, AssetManager assetManager, Node rootNode) {

		generate(assetManager, initialGalaxyCondition, initialStarCondition, numGalaxies);
		for (JGalaxy g : galaxies) {
			g.initStarSpheres(assetManager, rootNode);
		}
	}

	public void generate(AssetManager assetManager, InitialGalaxyConditionEnum initialGalaxyCondition,
			InitialStarConditionEnum initialStarCondition, int numGalaxies) {
		Physics.setFirstMove(true);
		switch (initialGalaxyCondition) {
		case random:
			galaxies = new JGalaxy[numGalaxies];
			for (int i = 0; i < numGalaxies; ++i) {
				JGalaxy g = new JGalaxy();
				galaxies[i] = g;

				initRandomGalaxyMassStarsRadius(galaxies[i]);
				// for first galaxy, set
				// initial position = 0;
				// initial speed = 0;
				// no translation or rotation
				if (i != 0) {
					setRandomInitialVelocity(galaxies[i]);
				}
			}
			break;
		case binaryOrbit:
			galaxies = new JGalaxy[2];
			galaxies[0] = new JGalaxy();
			galaxies[1] = new JGalaxy();
			initRandomGalaxyMassStarsRadius(galaxies[0]);
			initRandomGalaxyMassStarsRadius(galaxies[1]);
			float sumRadius = galaxies[0].getRadius() + galaxies[1].getRadius();
			float minRadius = Math.min(galaxies[0].getRadius(), galaxies[1].getRadius());

			binarySeparationDistance = Physics.randGen.nextFloat() * (sumRadius) + minRadius;
			logger.info("r1 = " + galaxies[0].getRadius() + " r2 = " + galaxies[1].getRadius() + " sumRadius = "
					+ sumRadius + " binarySeparationDistance = " + binarySeparationDistance);

			float m0 = galaxies[0].getMass();
			float m1 = galaxies[1].getMass();

			float v0 = (float) Math.sqrt(Physics.G * m1 * m1 / (m0 + m1) / binarySeparationDistance);
			float v1 = (float) Math.sqrt(Physics.G * m0 * m0 / (m0 + m1) / binarySeparationDistance);

			galaxies[0].setVelocity(new Vector3f(0f, -v0, 0));
			galaxies[1].setVelocity(new Vector3f(0f, v1, 0));

			break;
		}

		for (JGalaxy g : galaxies) {
			switch (initialStarCondition) {
			case randomDisk:

				initRandomStarDiscPositions(assetManager, g);
				initCircularVelocities(g);
				break;
			case linearStarDistribution:
				initStarsInLine(assetManager, g);
				break;
			case oneStar:
				initOneStarGalaxy(assetManager, g);
				break;
			}
		}

		// now that disc has been created,
		// rotate and translate entire galaxy
		// don't rotate or translate for first galaxy
		switch (initialGalaxyCondition) {
		case random:
			// don't move first galaxy
			for (int i = 1; i < galaxies.length; ++i) {
				rotateRandom(galaxies[i]);
				translateRandom(galaxies[i]);
			}
			break;
		case binaryOrbit:
			translate(galaxies[1], new Vector3f(binarySeparationDistance, 0, 0));
			break;
		}

		logger.info("galaxy 0 stars: " + galaxies[0].getStars().length + " " + galaxies[0]);
		logger.info("galaxy 1 stars: " + galaxies[1].getStars().length + galaxies[1]);
	}

	public static void initRandomGalaxyMassStarsRadius(JGalaxy g) {
		int numStars = Physics.randGen.nextInt(Physics.maxStars - Physics.minStars + 1) + Physics.minStars;
		g.setStars(new Star[numStars]);
		logger.info("created stars array numStars = " + g.getStars().length);

		logger.info("rSoft = " + Physics.rSoft);
		logger.info("rmin = " + Physics.minGalaxyRadius);

		// set mass and radius
		// use same rand - bigger mass, bigger galaxy
		float rand = Physics.randGen.nextFloat();
		g.setMass(rand * (Physics.maxGalaxyMass - Physics.minGalaxyMass) + Physics.minGalaxyMass);
		g.setRadius(rand * (Physics.maxGalaxyRadius - Physics.minGalaxyRadius) + Physics.minGalaxyRadius);

		g.setStarColor(ColorRGBA.randomColor());

	}

	public static void initRandomStarDiscPositions(AssetManager assetManager, JGalaxy g) {

		for (int i = 0; i < g.getStars().length; ++i) {
			// float r = ((randGen.nextFloat() + 1000f * rSoft) * radius);
			float r = Physics.randGen.nextFloat() * (g.getRadius() - Physics.minGalaxyRadius) + Physics.minGalaxyRadius;
			float phi = (float) (Math.PI / 2.0d);
			float theta = (float) (Physics.randGen.nextFloat() * 2f * Math.PI);
			// logger.info("r = " + r + " theta = " + theta + " phi = " + phi);
			SphericalCoordinates sp = new SphericalCoordinates(r, theta, phi);
			Vector3f spf = new Vector3f((float) sp.getCartesian().getX() + g.getPosition().getX(),
					(float) sp.getCartesian().getY() + g.getPosition().getY(),
					(float) sp.getCartesian().getZ() + g.getPosition().getZ());

			Vector3f v3d = new Vector3f(0f, 0f, 0f);

			Star s = new Star(assetManager, spf, v3d, g.getStarMass(), g.getStarColor());
			g.getStars()[i] = s;
		}
	}

	public static void setRandomInitialVelocity(JGalaxy g) {
		int vsign = Physics.randGen.nextInt(2) * 2 - 1;
		float gvx = vsign * Physics.randGen.nextFloat() * Physics.vscale;
		float gvy = vsign * Physics.randGen.nextFloat() * Physics.vscale;
		float gvz = vsign * Physics.randGen.nextFloat() * Physics.vscale;
		Vector3f vel = new Vector3f(gvx, gvy, gvz);
		g.setVelocity(vel);
	}

	public static void initCircularVelocities(JGalaxy g) {

		int vsign = Physics.randGen.nextInt(2) * 2 - 1;

		for (Star star : g.getStars()) {
			float r = star.getPosition().length();
			float x = star.getPosition().getX();
			float y = star.getPosition().getY();

			Vector3f force = Physics.computeForce(g, star);
			// logger.info("force = " + force);

			float theta = (float) Math.atan2(y, x);

			float v = (float) (vsign * Math.sqrt(r * force.length() / g.getStarMass()));
			float vx = (float) (v * Math.cos(-Math.PI / 2d + theta));
			float vy = (float) (v * Math.sin(-Math.PI / 2d + theta));
			int vsign2 = Physics.randGen.nextInt(2) * 2 - 1;
			float vz = vsign2 * (float) (Math.sqrt(vx * vx + vy * vy) * 0.1f) * Physics.randGen.nextFloat();

			Vector3f v3d = new Vector3f(vx, vy, vz);

			// add the galaxy velocity to the star velocity
			v3d = v3d.add(g.getVelocity());
			// logger.info("initial star velocity = " + v3d.toString());
			star.setVelocity(v3d);

			// set the initial acceleration
			// logger.info("G = " + Physics.G + " r = " + r + " x = " + x + " y = " + y + "
			// gmass = " + mass
			// + " starmass = " + starMass);
			Vector3f a3d = star.getPosition().mult(Physics.G * g.getMass() / r / r / r);
			// logger.info("accel = " + a3d.toString());
			star.setAcceleration(a3d);
		}
	}

	public static void initStarsInLine(AssetManager assetManager, JGalaxy g) {
		g.setMass(Physics.maxGalaxyMass);
		g.setStarColor(ColorRGBA.White);

		for (int i = 0; i < g.getStars().length; ++i) {
			Vector3f pos = new Vector3f(
					i * (Physics.maxGalaxyRadius - Physics.minGalaxyRadius) + Physics.minGalaxyRadius, 0f, 0f);
			Star s = new Star(assetManager, pos, g.getVelocity(), g.getStarMass(), ColorRGBA.White);
			g.getStars()[i] = s;
		}
	}

	public void initOneStarGalaxy(AssetManager assetManager, JGalaxy g) {
		g.setMass(Physics.maxGalaxyMass);
		g.setStarColor(ColorRGBA.White);

		Vector3f pos = new Vector3f(0.5f, 0f, 0f);
		g.setStars(new Star[1]);
		Star s = new Star(assetManager, pos, new Vector3f(0f, 0f, 0f), g.getStarMass(), ColorRGBA.White);
		g.getStars()[0] = s;
	}

	public void outputStarData(JGalaxy g) {
		logger.info("writing star position and velocity");
		FileWriter file;
		try {
			file = new FileWriter(starDataFileName);
			BufferedWriter writer = new BufferedWriter(file);

			for (Star s : g.getStars()) {
				writer.write(s.getPositionVelocityString() + "\n");
			}
			writer.close();
			logger.info("done writing star position and velocity");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void translate(JGalaxy g, Vector3f transVec) {
		Transform trans = new Transform(transVec);
		Vector3f translated = new Vector3f(0f, 0f, 0f);
		trans.transformVector(g.getPosition(), translated);
		g.setPosition(translated);

		for (Star s : g.getStars()) {
			Vector3f translated2 = new Vector3f(0f, 0f, 0f);
			trans.transformVector(s.getPosition(), translated2);
			s.setPosition(translated2);
		}
	}

	public void translateRandom(JGalaxy g) {
		float x = Physics.randGen.nextFloat() * (Physics.xmax - Physics.xmin) + Physics.xmin;
		float y = Physics.randGen.nextFloat() * (Physics.ymax - Physics.ymin) + Physics.ymin;
		float z = Physics.randGen.nextFloat() * (Physics.zmax - Physics.zmin) + Physics.zmin;
		Vector3f transVec = new Vector3f(x, y, z);
		translate(g, transVec);
	}

	public void rotateRandom(JGalaxy g) {
		Quaternion q = new Quaternion();
		float xAngle = (float) (Physics.randGen.nextFloat() * Math.PI * 2d);
		float yAngle = (float) (Physics.randGen.nextFloat() * Math.PI * 2d);
		// q.fromAngleAxis((float) (Math.PI / 4), new Vector3f(0f, 1f, 0f));
		q.fromAngles(xAngle, yAngle, 0f);

		Transform trans = new Transform(q);
		Vector3f translatedGalaxyPos = new Vector3f(0f, 0f, 0f);
		trans.transformVector(g.getPosition(), translatedGalaxyPos);
		g.setPosition(translatedGalaxyPos);

		Vector3f translatedGalaxyVel = new Vector3f(0f, 0f, 0f);
		trans.transformVector(g.getVelocity(), translatedGalaxyVel);
		g.setVelocity(translatedGalaxyVel);

		for (Star s : g.getStars()) {
			Vector3f translatedPos = new Vector3f(0f, 0f, 0f);
			trans.transformVector(s.getPosition(), translatedPos);
			s.setPosition(translatedPos);
			Vector3f translatedVel = new Vector3f(0f, 0f, 0f);
			trans.transformVector(s.getVelocity(), translatedVel);
			s.setVelocity(translatedVel);
		}
	}

	public JGalaxy[] getGalaxies() {
		return galaxies;
	}

}
