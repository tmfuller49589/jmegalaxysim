package sim;

public class ParticleData {
	float time;
	float Et;
	float Eg;
	float Ek;
	float r;
	float v;
	NewtonianObject newtonianObject;

	public ParticleData(NewtonianObject nobj, float time, float et, float eg, float ek, float r, float v) {
		super();
		this.newtonianObject = nobj;
		this.time = time;
		this.Et = et;
		this.Eg = eg;
		this.Ek = ek;
		this.r = r;
		this.v = v;
	}

	public NewtonianObject getNewtonianObject() {
		return newtonianObject;
	}

	public void setNewtonianObject(NewtonianObject newtonianObject) {
		this.newtonianObject = newtonianObject;
	}

	@Override
	public String toString() {
		String s = "t = " + time;
		s += "Et = " + Et;
		s += " Eg = " + Eg;
		s += " Ek = " + Ek;
		s += " r = " + r;
		s += " v = " + v;
		return s;
	}

	public String toCsv() {
		String s = time + ", ";
		s += Et + ", ";
		s += Eg + ", ";
		s += Ek + ", ";
		s += r + ", ";
		s += v;
		return s;
	}

	public float getTime() {
		return time;
	}

	public void setTime(float time) {
		this.time = time;
	}

	public float getEt() {
		return Et;
	}

	public void setEt(float et) {
		Et = et;
	}

	public float getEg() {
		return Eg;
	}

	public void setEg(float eg) {
		Eg = eg;
	}

	public float getEk() {
		return Ek;
	}

	public void setEk(float ek) {
		Ek = ek;
	}

	public float getR() {
		return r;
	}

	public void setR(float r) {
		this.r = r;
	}

	public float getV() {
		return v;
	}

	public void setV(float v) {
		this.v = v;
	}

}
