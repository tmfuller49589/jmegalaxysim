package jme3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Sphere;

import sim.Galaxy;
import sim.Physics;
import sim.Star;

public class JGalaxy extends Galaxy {

	public enum starPositionInitializationEnum {
		oneStar, nStar, randomDisc;
	}

	private static final Logger logger = LogManager.getLogger();
	private Mesh mesh = new Mesh();
	private Sphere blackHole = new Sphere(32, 32, Physics.rSoft, false, true);

	private Geometry geometry;
	private Geometry blackHoleGeom;
	private Node node;

	public JGalaxy() {
		super();
	}

	public void initStarSpheres(AssetManager assetManager, Node rootNode) {

		starsToLineVertices(assetManager, rootNode);
	}

	private void starsToLineVertices(AssetManager assetManager, Node rootNode) {
		logger.info("galaxy is " + this);
		for (Star s : stars) {
			float x = s.getPosition().x;
			float y = s.getPosition().y;
			float z = s.getPosition().z;

			// sphereGeo.setMaterial((Material)
			// assetManager.loadMaterial("Materials/MyCustomMaterial.j3m"));
			s.getGeom().setLocalTranslation(x, y, z);
			rootNode.attachChild(s.getGeom());

		}
	}

	public Mesh getMesh() {
		return mesh;
	}

	public void setMesh(Mesh mesh) {
		this.mesh = mesh;
	}

	public Geometry getBlackHoleGeom() {
		return blackHoleGeom;
	}

	public void setupPoints(AssetManager assetManager, Node rootNode) {
		mesh.setMode(Mesh.Mode.Points);

		geometry = new Geometry("Points", mesh);
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");

		mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
		mat.getAdditionalRenderState().setBlendEquation(RenderState.BlendEquation.Add);
		mat.getAdditionalRenderState().setBlendEquationAlpha(RenderState.BlendEquationAlpha.Max);

		mat.setColor("Color", starColor);
		geometry.setMaterial(mat);

		mesh.updateBound();
		mesh.updateCounts();
		rootNode.attachChild(geometry);

		blackHoleGeom = new Geometry("A shape", blackHole); // wrap shape into geometry

		// Material blackHoleMaterial = new Material(assetManager,
		// "Common/MatDefs/Misc/ShowNormals.j3md");
		Material blackHoleMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		blackHoleMaterial.setColor("Color", ColorRGBA.randomColor());

		blackHoleGeom.setMaterial(blackHoleMaterial); // assign material to geometry

		node = new Node("galaxyNode");
		node.attachChild(blackHoleGeom);

		rootNode.attachChild(node);
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public void updateBlackHoleGeometry(Vector3f positionChange) {
		// logger.info("position change = " + positionChange);
		// blackHoleGeom.move(positionChange);
		node.setLocalTranslation(getPosition());
	}
}
