package jme3;

// TODO
// options screen
// binary galaxy orbit
// save initial settings
// pan camera around center of mass

import java.nio.FloatBuffer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Transform;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.CameraNode;
import com.jme3.scene.control.CameraControl.ControlDirection;
import com.jme3.system.AppSettings;

import sim.InitialConditionGenerator;
import sim.Physics;
import sim.Physics.IntegrationMethodEnum;

public class GalaxySim extends SimpleApplication {

	public static final int numberOfGalaxies = 2;
	private FloatBuffer floatBuffer;

	private static final Logger logger = LogManager.getLogger();
	JGalaxy[] galaxies = new JGalaxy[numberOfGalaxies];
	private boolean isRunning = true;
	CameraNode camNode;
	float cameraMoveStep = 0.1f;
	boolean rotateCamera = false;
	float rotationAngleStep = (float) (Math.PI * 2d / 200d);
	BitmapText hudText;
	boolean hud = true;

	static int i = 0;

	public static void main(String[] args) {
		//

		GalaxySim app = new GalaxySim();
		AppSettings gameSettings = null;
		gameSettings = new AppSettings(false);
		gameSettings.setResolution(1280, 720);
		gameSettings.setFullscreen(false);
		gameSettings.setVSync(false);
		gameSettings.setTitle("JGalaxy Sim");
		gameSettings.setUseInput(true);
		// gameSettings.setFrameRate(500);
		gameSettings.setSamples(0);
		// gameSettings.setRenderer(“LWJGL-OpenGL2”);
		app.settings = gameSettings;
		// app.setShowSettings(false);
		app.start();
	}

	private void initKeys() {
		// You can map one or several inputs to one named action
		inputManager.addMapping("Pause", new KeyTrigger(KeyInput.KEY_P));
		inputManager.addMapping("CenterMass", new KeyTrigger(KeyInput.KEY_M));
		inputManager.addMapping("Initialize", new KeyTrigger(KeyInput.KEY_I));
		inputManager.addMapping("IncreaseCamSpeed", new KeyTrigger(KeyInput.KEY_ADD));
		inputManager.addMapping("DecreaseCamSpeed", new KeyTrigger(KeyInput.KEY_SUBTRACT));
		inputManager.addMapping("Initialize", new KeyTrigger(KeyInput.KEY_I));
		inputManager.addMapping("HudToggle", new KeyTrigger(KeyInput.KEY_H));

		inputManager.addMapping("Forward", new KeyTrigger(KeyInput.KEY_W));
		inputManager.addMapping("Backward", new KeyTrigger(KeyInput.KEY_S));
		inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_Q));
		inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_Z));
		inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_A));
		inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_D));

		inputManager.addMapping("One", new KeyTrigger(KeyInput.KEY_1));
		inputManager.addMapping("Two", new KeyTrigger(KeyInput.KEY_2));
		inputManager.addMapping("Three", new KeyTrigger(KeyInput.KEY_3));
		inputManager.addMapping("Rotate", new KeyTrigger(KeyInput.KEY_R),
				new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
		// Add the names to the action listener.
		inputManager.addListener(actionListener, "Pause");
		inputManager.addListener(analogListener, "Forward", "Backward", "Up", "Down", "Left", "Right", "Initialize",
				"Rotate", "Output", "CenterMass", "Close", "One", "Two", "Three", "IncreaseCamSpeed",
				"DecreaseCamSpeed", "HudToggle");
	}

	private final ActionListener actionListener = new ActionListener() {
		@Override
		public void onAction(String name, boolean keyPressed, float tpf) {
			if (name.equals("Pause") && !keyPressed) {
				isRunning = !isRunning;
			}
		}
	};

	private final AnalogListener analogListener = new AnalogListener() {
		@Override
		public void onAnalog(String name, float value, float tpf) {
			if (isRunning) {
				if (name.equals("Initialize")) {
					logger.info("initialize");
					initializeGalaxies();
				} else if (name.equals("IncreaseCamSpeed")) {
					cameraMoveStep += .1;
					logger.info("cameraMoveStep = " + cameraMoveStep);
				} else if (name.equals("DecreaseCamSpeed")) {
					cameraMoveStep -= .1;
					cameraMoveStep = Math.abs(cameraMoveStep);
					cameraMoveStep = (float) Math.max(cameraMoveStep, .01);
					logger.info("cameraMoveStep = " + cameraMoveStep);
				} else if (name.equals("Rotate")) {
					rotateCamera = !rotateCamera;
					logger.info("cameraMoveStep = " + cameraMoveStep);
				} else if (name.equals("Right")) {
					camNode.setLocalTranslation(
							camNode.getLocalTranslation().add(new Vector3f(cameraMoveStep, 0f, 0f)));
				} else if (name.equals("Left")) {
					camNode.setLocalTranslation(
							camNode.getLocalTranslation().add(new Vector3f(-cameraMoveStep, 0f, 0f)));
				} else if (name.equals("Up")) {
					camNode.setLocalTranslation(
							camNode.getLocalTranslation().add(new Vector3f(0f, cameraMoveStep, 0f)));
				} else if (name.equals("Down")) {
					camNode.setLocalTranslation(
							camNode.getLocalTranslation().add(new Vector3f(0f, -cameraMoveStep, 0f)));
				} else if (name.equals("Forward")) {
					camNode.setLocalTranslation(
							camNode.getLocalTranslation().add(new Vector3f(0f, 0f, cameraMoveStep)));
				} else if (name.equals("Backward")) {
					camNode.setLocalTranslation(
							camNode.getLocalTranslation().add(new Vector3f(0f, 0f, -cameraMoveStep)));
				} else if (name.equals("CenterMass")) {
					Vector3f cm = Physics.computeCenterOfMass(galaxies);
				} else if (name.equals("HudToggle")) {
					hudToggle();
				} else if (name.equals("One")) {
					// Attach the camNode to the target:
					logger.info("attachign camera to galaxy 1");
					galaxies[0].getNode().attachChild(camNode);
					// Move camNode, e.g. behind and above the target:
					camNode.setLocalTranslation(new Vector3f(0, 0, -5));
					// Rotate the camNode to look at the target:
					camNode.lookAt(galaxies[0].getNode().getLocalTranslation(), Vector3f.UNIT_Y);

				} else if (name.equals("Two")) {
					if (galaxies.length >= 2) {
						logger.info("attachign camera to galaxy 2");
						// Attach the camNode to the target:
						galaxies[1].getNode().attachChild(camNode);
						// Move camNode, e.g. behind and above the target:
						camNode.setLocalTranslation(new Vector3f(0, 0, -5));
						// Rotate the camNode to look at the target:
						camNode.lookAt(galaxies[1].getNode().getLocalTranslation(), Vector3f.UNIT_Y);
					}
				} else if (name.equals("Three")) {
					if (galaxies.length >= 3) {
						logger.info("attachign camera to galaxy 3");
						// Attach the camNode to the target:
						galaxies[2].getNode().attachChild(camNode);
						// Move camNode, e.g. behind and above the target:
						camNode.setLocalTranslation(new Vector3f(0, 0, -5));
						// Rotate the camNode to look at the target:
						camNode.lookAt(galaxies[1].getNode().getLocalTranslation(), Vector3f.UNIT_Y);
					}
				}
			} else {
				System.out.println("Press P to unpause.");
			}
		}
	};

	@Override
	public void destroy() {
		super.destroy();
		Physics.closeParticleDataFile();
	}

	public void hudToggle() {
		hud = !hud;
		if (hud) {
			guiNode.getChildren().add(hudText);
		} else {
			guiNode.getChildren().remove(hudText);
		}
	}

	@Override
	public void simpleInitApp() {
		flyCam.setEnabled(false);
		setDisplayStatView(false);
		setDisplayFps(false);

		hudText = new BitmapText(guiFont, false);
		hudText.setSize(guiFont.getCharSet().getRenderedSize()); // font size
		hudText.setColor(ColorRGBA.White); // font color
		String text = "hotkeys" + System.lineSeparator();
		text += "AWSDQZ move" + System.lineSeparator();
		text += "+- cam speed" + System.lineSeparator();
		text += "I initialize" + System.lineSeparator();
		text += "P pause" + System.lineSeparator();
		text += "12 follow galaxy" + System.lineSeparator();
		text += "M center of mass" + System.lineSeparator();
		text += "H display hotkey" + System.lineSeparator();
		text += "ESC quit" + System.lineSeparator();

		hudText.setText(text); // the text
		hudText.setLocalTranslation(100, 500, 0); // position

		guiNode.attachChild(hudText);

		// create the camera Node
		camNode = new CameraNode("Camera Node", cam);

		initKeys();

		Physics.setIntegrationMethod(IntegrationMethodEnum.leapfrog);
		initializeGalaxies();

	}

	public void initializeGalaxies() {
		rotateCamera = false;
		// InitialConditionGenerator icg = new
		// InitialConditionGenerator(InitialGalaxyConditionEnum.random,
		// InitialStarConditionEnum.randomDisk, numberOfGalaxies);
		rootNode.detachAllChildren();

		InitialConditionGenerator icg = new InitialConditionGenerator(assetManager, rootNode, numberOfGalaxies);

		galaxies = icg.getGalaxies();
		for (JGalaxy g : galaxies) {
			g.setupPoints(assetManager, rootNode);
		}

		// This mode means that camera copies the movements of the target:
		camNode.setControlDir(ControlDirection.SpatialToCamera);
		// Attach the camNode to the target:
		galaxies[0].getNode().attachChild(camNode);
		// Move camNode, e.g. behind and above the target:
		camNode.setLocalTranslation(new Vector3f(0, 0, -7));
		// Rotate the camNode to look at the target:
		camNode.lookAt(galaxies[0].getNode().getLocalTranslation(), Vector3f.UNIT_Y);
	}

	@Override
	public void simpleUpdate(float tpf) {

		Physics.move(galaxies);
		// Physics.writeParticleData(galaxies);

		if (rotateCamera) {
			rotateCamera();
		}
	}

	private void rotateCamera() {
		Quaternion addAngle = new Quaternion();
		addAngle.fromAngleAxis(rotationAngleStep, Vector3f.UNIT_Y);

		Vector3f camNodeLocation = camNode.getLocalTranslation();
		Transform trans = new Transform(addAngle);
		Vector3f camNodeLocationNew = new Vector3f(0f, 0f, 0f);
		trans.transformVector(camNodeLocation, camNodeLocationNew);
		camNode.setLocalTranslation(camNodeLocationNew);
		camNode.lookAt(galaxies[0].getNode().getLocalTranslation(), Vector3f.UNIT_Y);

	}

	@Override
	public void simpleRender(RenderManager rm) {
		for (JGalaxy g : galaxies) {
			// floatBuffer = BufferUtils.createFloatBuffer(g.getLineVertices());
			// g.getMesh().setBuffer(VertexBuffer.Type.Position, 3, floatBuffer);
			// g.getMesh().updateBound();
		}
	}
}