package jme3;

import com.jme3.math.Transform;
import com.jme3.math.Vector3f;

public class TransformTest {
	public static void main(String[] args) {
		float x = 1f;
		float y = 2f;
		float z = 3f;
		Vector3f orig = new Vector3f(x, y, z);

		Transform trans = new Transform(new Vector3f(2, 4, 6));
		Vector3f translated = new Vector3f(0f, 0f, 0f);

		trans.transformVector(orig, translated);

		System.out.println("original = " + orig.toString());
		System.out.println("translated = " + translated.toString());
	}
}
